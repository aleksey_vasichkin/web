<footer class="container pt-4 my-md-5 pt-md-5 border-top">
    <div class="row">
        <div class="col-12 col-md">
            <small class="d-block mb-3 text-muted">© 2017–2021</small>
        </div>
        <div class="col-6 col-md">
            <h5>Социальные сети</h5>
            <ul class="list-unstyled text-small">
                <li><a class="link-secondary" href="#">Вконтакте</a></li>
                <li><a class="link-secondary" href="#">Facebook</a></li>
                <li><a class="link-secondary" href="#">Twitter</a></li>
                <li><a class="link-secondary" href="#">Одноклассники</a></li>
            </ul>
        </div>
        <div class="col-6 col-md">
            <h5>О нас</h5>
            <ul class="list-unstyled text-small">
                <li><a class="link-secondary" href="#">История</a></li>
                <li><a class="link-secondary" href="#">Политика конфиденциальности</a></li>
                <li><a class="link-secondary" href="#">Контактная информация</a></li>
            </ul>
        </div>
        <div class="col-6 col-md">
            <h5>Документы Chicken-call</h5>
            <ul class="list-unstyled text-small">
                <li><a class="link-secondary" href="#">Устав</a></li>
                <li><a class="link-secondary" href="#">Положение о совете директоров</a></li>
                <li><a class="link-secondary" href="#">Положение об общем собрании акционеров</a></li>
                <li><a class="link-secondary" href="#">Свидетельство о регистрации</a></li>
            </ul>
        </div>
    </div>
</footer>