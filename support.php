<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=egde">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <title>Техническая поддержка</title>
</head>
<body>

<?php require "blocks/header.php"?>
<div class="container mt-5">
    <h3>Техническая поддержка</h3>
    <form action="check.php" method="post">
        <input type="email" name="email" placeholder="Введите Email" class="form-control"><br>
        <textarea name="message" class="form-control" placeholder="Введите ваше сообщение"></textarea><br>
        <button type="submit" class="w-100 btn btn-lg btn-outline-primary">Отправить</button>
    </form>
</div>


<?php require "blocks/footer.php"?>

</body>
</html>
